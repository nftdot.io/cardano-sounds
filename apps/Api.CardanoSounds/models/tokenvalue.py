from dataclasses import dataclass

@dataclass
class TokenValue:
    unit: str
    quantity: str
    